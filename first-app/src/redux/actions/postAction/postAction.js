import { GET_POST, UPDATE_POST, DELETE_POST } from "./postActionType";

export const getPosts = () => {
    // Axios.get('https://jsonplaceholder.typicode.com/posts')
    console.log("======> Get Post");
    return {
      type: GET_POST,
      data: [
        {
          userId: 1,
          id: 1,
          title:
            "ABC",
          body:
            "ABC",
        },
        {
          userId: 1,
          id: 2,
          title: "123",
          body:
            "123",
        },
      ],
    };
  };
  